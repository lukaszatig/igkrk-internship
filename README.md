# IG Internship

This repo contains a simple web app that we would like you to modify.

Please read contents of this file carefully. Pieces of code that are already there should help You to solve these tasks.
If any of these instructions are unclear, we’ll be happy if you make a decision yourself. Then note down what was unclear, what you decided and why.

Still if it is imposssible to understand, please contact lukasz.dospial@ig.com.

You have one week to deal with these tasks. If You don't complete them all, don't worry, just commit what have you acomplished so far.

## Tasks ##

Completing all of these tasks should not take more than few hours.

Please modify code in index.html, main.js or main.css.

Website do not need any server to run, just open index.html in a modern browser.

### Display data ###
Some data is missing.

* Column with age is not visible, please make it appear.
* User wants to this data to be pretty:
    * table should be 720px of maximum width
    * table rows should be alternatly coloured (ie, first -rose, second - violet, third - rose, ...)
    * table and header should be centered
    * if you need an classes to be added please follow naming convention
* if value is empty or missing user should just see empty space 

### Mainpulate data ###

After a while user decided that some new data is needed

* please display age in years just after age in months
* replace name with full name of a dog, which is name plus familly name

### State ###

User wants random dogs to be added to list, 
* Please make it happen after clicking "Add a dog" button. Dog should be different each time, list should be refreshed

### Actions and dom ###

User wants to have images of each dog

* add new column witch action button or click action on row that will allow to display image in provided container
* when clicked it should render an image in a container
* on age header add two buttons ( &#x25B2;, &#x25BC;), depending which is clicked it should call action that will sort data by age, ascending or descendig 

### Bonus

* add loader when waiting for data
* make image preview a modal
* make table even more pretty
* sort by full name
* can You make table render faster?


